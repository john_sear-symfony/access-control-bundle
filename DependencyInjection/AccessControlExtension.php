<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\DependencyInjection;

use JohnSear\AccessControlBundle\Fixtures\Base\AbstractFixtures;
use JohnSear\AccessControlBundle\Resources\fixtures\SystemUserFixture;
use JohnSear\AccessControlBundle\Security\FormLoginAuthenticator;
use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\BadMethodCallException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AccessControlExtension extends Extension
{
    /** @var ContainerBuilder */
    private $container;

    /**
     * @throws Exception
     */
    public function load(array $config, ContainerBuilder $container)
    {
        $this->container = $container;

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}
