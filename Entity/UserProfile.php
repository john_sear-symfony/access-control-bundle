<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JohnSear\AccessControlBundle\Logging\AbstractCreationAndUpdateLoggingEntity;

/**
 * @ORM\Entity(repositoryClass="JohnSear\AccessControlBundle\Repository\UserProfileRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class UserProfile extends AbstractCreationAndUpdateLoggingEntity implements FixtureEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="JohnSear\AccessControlBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(type="string")
     */
    private $firstName;

    /**
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateOfBirth;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ? User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFirstName(): string
    {
        return (string) $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return (string) $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDateOfBirth(): ? \DateTime
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(\DateTime $date): self
    {
        $this->dateOfBirth = $date;

        return $this;
    }
}
