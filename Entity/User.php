<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="JohnSear\AccessControlBundle\Repository\UserRepository")
 * @UniqueEntity(fields="email", message="johnsear.acc.user.email.unique")
 */
class User implements UserInterface, FixtureEntityInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="array")
     */
    private $roles;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->getRolesAsCollection();

        return $roles->toArray();
    }

    public function setRoles(array $roles): self
    {
        foreach ($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    public function addRole(string $role): self
    {
        $this->roles->add($role);

        return $this;
    }

    public function removeRole(string $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }

    public function getRolesAsCollection(): Collection
    {
        $roles = $this->roles;

        $this->guaranteeRolesContainsRoleUser($roles);
        $this->guaranteeRolesAreUnique($roles);

        return $roles;
    }

    private function guaranteeRolesContainsRoleUser(Collection &$roles): void
    {
        $roles->add('ROLE_USER');
    }

    private function guaranteeRolesAreUnique(Collection &$roles): void
    {
        $uniqueRoles = new ArrayCollection();

        $uniqueRolesArray = array_unique($roles->toArray());
        foreach ($uniqueRolesArray as $role) {
            $uniqueRoles->add($role);
        }

        $roles = $uniqueRoles;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}
