<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use JohnSear\AccessControlBundle\Entity\User;
use JohnSear\AccessControlBundle\Form\RegistrationFormType;
use JohnSear\AccessControlBundle\Form\UserType;
use JohnSear\AccessControlBundle\Repository\UserRepository;
use JohnSear\AccessControlBundle\Security\FormLoginAuthenticator;
use OutOfBoundsException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

/**
 * @Route("/user", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->redirectToRoute('access_control_user_list');
    }

    /**
     * @Route("/list", name="list", methods={"GET"})
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @throws \LogicException
     */
    public function list(UserRepository $userRepository): Response
    {
        return $this->render('@AccessControl/user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @throws LogicException
     * @throws \LogicException
     * @throws OutOfBoundsException
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                // encode the plain password
                $user->setPassword(
                    $this->passwordEncoder->encodePassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                $this->addFlash('success', 'johnsear.save.success');

                return $this->redirectToRoute('access_control_user_index');
            } else {
                $this->addFlash('danger', 'johnsear.save.failed');
            }
        }

        return $this->render('@AccessControl/user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register", name="register")
     *
     * @IsGranted("IS_AUTHENTICATED_ANONYMOUSLY")
     */
    public function register(Request $request, GuardAuthenticatorHandler $guardHandler, FormLoginAuthenticator $authenticator): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email

            return $guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $authenticator,
                'main' // firewall name in security.yaml
            );
        }

        return $this->render('@AccessControl/user/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @throws \LogicException
     */
    public function show(User $user): Response
    {
        return $this->render('@AccessControl/user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     *
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     *
     * @throws LogicException
     * @throws \LogicException
     * @throws OutOfBoundsException
     */
    public function edit(Request $request, User $user): Response
    {
        $dontChangeMePlaceholder = $this->prepareRequestForPasswordChangeOnEdit($request);
        $originalRoles = $user->getRolesAsCollection();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                /** encode the plain password if changed */
                if($form->get('plainPassword')->getData() != $dontChangeMePlaceholder) {
                    $user->setPassword(
                        $this->passwordEncoder->encodePassword(
                            $user,
                            $form->get('plainPassword')->getData()
                        )
                    );
                }

                /** remove role if deleted in form */
                foreach ($originalRoles->toArray() as $role) {
                    if (false === $user->getRolesAsCollection()->contains($role)) {
                        $user->removeRole($role);
                    }
                }

                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', 'johnsear.update.success');

                return $this->redirectToRoute('access_control_user_index');
            } else {
                $this->addFlash('danger', 'johnsear.update.failed');
            }
        }

        return $this->render('@AccessControl/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'collapse_password' => true,
        ]);
    }

    private function prepareRequestForPasswordChangeOnEdit(Request $request, $dontChangeMePlaceholder = 'dontChangeMe'): string
    {
        $requestAll = $request->request->all();
        $requestFormData = (is_array($requestAll) && count($requestAll) && array_key_exists('user',$requestAll) && is_array($requestAll['user'])) ? $requestAll['user'] : [];

        if(count($requestFormData) && array_key_exists('plainPassword', $requestFormData) && $requestFormData['plainPassword'] === '') {
            $requestFormData['plainPassword'] = $dontChangeMePlaceholder;

            $request->request->set('user', $requestFormData);
        }

        return $dontChangeMePlaceholder;
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     *
     * @IsGranted("ROLE_ADMIN")
     *
     * @throws \LogicException
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();

            $this->addFlash('success', 'johnsear.delete.success');
        } else {
            $this->addFlash('danger', 'johnsear.delete.failed');
        }

        return $this->redirectToRoute('access_control_user_index');
    }
}
