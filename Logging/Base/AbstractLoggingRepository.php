<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Base;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use JohnSear\AccessControlBundle\Exception\NotAuthenticatedException;
use JohnSear\AccessControlBundle\Exception\NoValidUserAuthenticatedException;
use JohnSear\AccessControlBundle\UserResolver\UserResolverInterface;
use JohnSear\AccessControlBundle\Entity\User;

abstract class AbstractLoggingRepository extends ServiceEntityRepository
    implements LoggingRepositoryInterface
{
    private $userResolver;

    public function __construct(
        ManagerRegistry $registry,
        UserResolverInterface $userResolver
    )
    {
        parent::__construct($registry, $this->getEntityClass());
        $this->userResolver = $userResolver;
    }

    abstract public function getEntityClass(): string;

    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    public function setCurrentUserIfNull(LoggingEntityInterface $entity): void
    {
        if (!$entity->getCurrentUser() instanceof User) {
            $entity->setCurrentUser($this->userResolver->getCurrentUser());
        }
    }
}
