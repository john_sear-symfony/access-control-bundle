<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Base;

use JohnSear\AccessControlBundle\Exception\NotAuthenticatedException;
use JohnSear\AccessControlBundle\Exception\NoValidUserAuthenticatedException;

interface LoggingRepositoryInterface
{
    public function getEntityClass(): string;

    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    public function setCurrentUserIfNull(LoggingEntityInterface $entity): void;
}
