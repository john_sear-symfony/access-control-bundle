<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Base;

use JohnSear\AccessControlBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
abstract class AbstractLoggingEntity implements LoggingEntityInterface
{
    private $currentUser;

    public function getCurrentUser(): ?User
    {
        return $this->currentUser;
    }

    public function setCurrentUser(User $user): LoggingEntityInterface
    {
        $this->currentUser = $user;

        return $this;
    }
}
