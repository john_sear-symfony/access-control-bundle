<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Base;

use JohnSear\AccessControlBundle\Entity\User;

interface LoggingEntityInterface
{
    public function getCurrentUser(): ?User;
    public function setCurrentUser(User $user): LoggingEntityInterface;
}
