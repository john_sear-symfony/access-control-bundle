<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging;

use JohnSear\AccessControlBundle\Logging\Base\AbstractLoggingRepository;
use JohnSear\AccessControlBundle\Logging\Traits\RepositoryCreationTrait;
use JohnSear\AccessControlBundle\Logging\Traits\RepositoryUpdateTrait;

abstract class AbstractCreationAndUpdateLoggingRepository extends AbstractLoggingRepository
    implements CreationAndUpdateLoggingRepositoryInterface
{
    use RepositoryCreationTrait;
    use RepositoryUpdateTrait;
}
