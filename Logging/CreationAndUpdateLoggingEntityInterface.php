<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging;

use JohnSear\AccessControlBundle\Logging\Traits\EntityCreationInterface;
use JohnSear\AccessControlBundle\Logging\Traits\EntityUpdateInterface;

interface CreationAndUpdateLoggingEntityInterface extends EntityCreationInterface, EntityUpdateInterface
{

}
