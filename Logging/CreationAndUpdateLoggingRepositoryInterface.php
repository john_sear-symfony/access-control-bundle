<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging;

use JohnSear\AccessControlBundle\Logging\Traits\RepositoryCreationInterface;
use JohnSear\AccessControlBundle\Logging\Traits\RepositoryUpdateInterface;

interface CreationAndUpdateLoggingRepositoryInterface extends RepositoryCreationInterface, RepositoryUpdateInterface
{

}
