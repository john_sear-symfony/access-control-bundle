<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging;

use JohnSear\AccessControlBundle\Logging\Base\AbstractLoggingEntity;
use JohnSear\AccessControlBundle\Logging\Traits\EntityCreationTrait;
use JohnSear\AccessControlBundle\Logging\Traits\EntityUpdateTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
abstract class AbstractCreationAndUpdateLoggingEntity extends AbstractLoggingEntity
    implements CreationAndUpdateLoggingEntityInterface
{
    use EntityCreationTrait;
    use EntityUpdateTrait;
}
