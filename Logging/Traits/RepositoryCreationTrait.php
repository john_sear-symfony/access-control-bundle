<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Traits;

use JohnSear\AccessControlBundle\Logging\Base\LoggingEntityInterface;
use JohnSear\AccessControlBundle\Logging\Base\LoggingRepositoryInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
trait RepositoryCreationTrait
{
    public function beforeInsert(LoggingEntityInterface $entity): void
    {
        /** @var LoggingRepositoryInterface $this */
        /** @var LoggingEntityInterface $entity */
        $this->setCurrentUserIfNull($entity);
    }
}
