<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Traits;

use JohnSear\AccessControlBundle\Entity\User;
use JohnSear\AccessControlBundle\Logging\Base\LoggingEntityInterface;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\HasLifecycleCallbacks()
 */
trait EntityCreationTrait
{
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="JohnSear\AccessControlBundle\Entity\User")
     */
    protected $createdBy;

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt): EntityCreationInterface
    {
        $this->createdAt = $createdAt;

        /** @var EntityCreationInterface $this */
        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(User $user): EntityCreationInterface
    {
        $this->createdBy = $user;

        /** @var EntityCreationInterface $this */
        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface
    {
        /** @var EntityCreationInterface $entity */
        $entity = $eventArgs->getEntity();

        $now = (new DateTime('now'))->setTimezone(new \DateTimeZone('Europe/Berlin'));

        return $entity->setCreatedAt($now);
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedByValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface
    {
        /** @var EntityCreationInterface $entity */
        $entity = $eventArgs->getEntity();

        /** @var LoggingEntityInterface $userLoggingEntity */
        $userLoggingEntity = $entity;

        /** @var RepositoryCreationInterface $loggingRepository */
        $loggingRepository = $eventArgs->getEntityManager()->getRepository(get_class($entity));

        $loggingRepository->beforeInsert($userLoggingEntity);

        return $entity->setCreatedBy($userLoggingEntity->getCurrentUser());
    }
}
