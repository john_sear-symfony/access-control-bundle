<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Traits;

use JohnSear\AccessControlBundle\Logging\Base\LoggingEntityInterface;

interface RepositoryCreationInterface
{
    public function beforeInsert(LoggingEntityInterface $entity): void;
}
