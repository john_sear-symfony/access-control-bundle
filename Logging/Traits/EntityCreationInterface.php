<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Traits;

use JohnSear\AccessControlBundle\Entity\User;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;

interface EntityCreationInterface
{
    public function getCreatedAt(): DateTime;
    public function setCreatedAt(DateTime $createdAt): EntityCreationInterface;

    public function getCreatedBy(): ?User;
    public function setCreatedBy(User $user): EntityCreationInterface;

    public function setCreatedAtValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface;

    public function setCreatedByValueOnCreation(LifecycleEventArgs $eventArgs): EntityCreationInterface;
}
