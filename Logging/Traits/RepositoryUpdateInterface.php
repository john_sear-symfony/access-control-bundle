<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Traits;

use JohnSear\AccessControlBundle\Logging\Base\LoggingEntityInterface;

interface RepositoryUpdateInterface
{
    public function beforeUpdate(LoggingEntityInterface $entity): void;
}
