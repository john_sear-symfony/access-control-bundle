<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Logging\Traits;

use JohnSear\AccessControlBundle\Entity\User;
use DateTime;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

interface EntityUpdateInterface
{
    public function getUpdatedAt(): DateTime;
    public function setUpdatedAt(DateTime $updatedAt): EntityUpdateInterface;

    public function getUpdatedBy(): ?User;
    public function setUpdatedBy(User $user): EntityUpdateInterface;

    public function setUpdatedByValueOnCreation(LifecycleEventArgs $eventArgs): EntityUpdateInterface;
    public function setUpdatedByValueOnUpdate(PreUpdateEventArgs $eventArgs): EntityUpdateInterface;

    public function setUpdatedAtValueOnCreation(LifecycleEventArgs $eventArgs): EntityUpdateInterface;
    public function setUpdatedAtValueOnUpdate(PreUpdateEventArgs $eventArgs): EntityUpdateInterface;
}
