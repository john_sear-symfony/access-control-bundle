<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Repository;

use JohnSear\AccessControlBundle\Entity\User;
use JohnSear\AccessControlBundle\Entity\UserProfile;
use Doctrine\Common\Persistence\ManagerRegistry;
use JohnSear\AccessControlBundle\Logging\AbstractCreationAndUpdateLoggingRepository;
use JohnSear\AccessControlBundle\UserResolver\UserResolverInterface;

/**
 * @method UserProfile|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserProfile|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserProfile[]    findAll()
 * @method UserProfile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserProfileRepository extends AbstractCreationAndUpdateLoggingRepository
{
    public function __construct(ManagerRegistry $registry, UserResolverInterface $userResolver)
    {
        parent::__construct($registry, $userResolver);
    }

    public function getEntityClass(): string
    {
        return UserProfile::class;
    }

    public function findOneByUser(User $user): ? UserProfile
    {
        return $this->findOneBy(['user' => $user->getId()]);
    }
}
