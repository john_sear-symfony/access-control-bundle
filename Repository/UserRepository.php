<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Repository;

use JohnSear\AccessControlBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function findOneByEmail(string $email): ?User
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function findByEmail(string $email): array
    {
        return $this->findBy(['email' => $email]);
    }

    public function findByRole(string $role): array
    {
        $qb = $this->createQueryBuilder('user')
            ->select('user')
            ->setParameter('role', '%' . $role . '%');
        $qb->where($qb->expr()->like('user.roles', ':role')); /** @TODO: Find a better Way to search inside ArrayCollection than "LIKE" */
        $query = $qb->getQuery();

        return $query->getResult();
    }
}
