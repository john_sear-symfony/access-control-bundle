# Access Control Bundle
(c) 2019 by [John_Sear](https://bitbucket.org/john_sear-symfony/access-control-bundle/)

## Symfony Bundle

This is a Symfony Bundle to control access in your symfony application.  
Be sure you are using symfony version 4.4.

> This Bundle is still in Development. Some things can be broken ;-)

## Installation

### via CLI
Run ``composer require johnsear/access-control-bundle:"^0.1.5"`` command in cli to install source starting from Version 0.1.5

### composer.json
Add following to your symfony application composer json file:
```json
{
  "require": {
    "johnsear/access-control-bundle": "^0.1.5"
  }
}
```

## Configuration

### Routing
Copy  ``\vendor\johnsear\access-control-bundle\Resources\config\routes\johnsear-access-control.yaml.dist`` _without_ suffix ``.dist`` into ``\config\routes\`` Folder
 
### Security
Add these information to your security.yaml file

```yaml
security:
    encoders:
        JohnSear\AccessControlBundle\Entity\User:
            algorithm: auto

    # https://symfony.com/doc/current/security.html#where-do-users-come-from-user-providers
    providers:
        # used to reload user from session & other features (e.g. switch_user)
        app_user_provider:
            entity:
                class: JohnSear\AccessControlBundle\Entity\User
                property: email
    firewalls:
        # ..
        main:
            pattern:  ^/
            anonymous: ~

            guard:
                authenticators:
                    - JohnSear\AccessControlBundle\Security\FormLoginAuthenticator
            logout:
                path: access_control_logout
                # where to redirect after logout
                target: access_control_login

            remember_me:
                secret:   '%kernel.secret%'
                lifetime: 604800 # 1 week in seconds
                path:     /

# ..
```

### Register all Services

For now, all services must be auto wired via the services.yaml.

Add following Lines at the end of ``config\services.yaml``

```yaml
services:

    # ..

    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    JohnSear\AccessControlBundle\:
        resource: '../vendor/johnsear/access-control-bundle/*'
# ..
```
