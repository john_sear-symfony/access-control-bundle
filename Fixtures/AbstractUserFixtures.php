<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Fixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use JohnSear\AccessControlBundle\Fixtures\Base\AbstractFixtures;
use JohnSear\AccessControlBundle\Resources\fixtures\SystemUserFixture;

abstract class AbstractUserFixtures extends AbstractFixtures implements DependentFixtureInterface
{
    public function getDependencies()
    {
        return array(
            SystemUserFixture::class,
        );
    }
}
