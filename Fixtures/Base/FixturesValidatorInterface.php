<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Fixtures\Base;

use Doctrine\Common\Persistence\ObjectManager;
use JohnSear\AccessControlBundle\Entity\FixtureEntityInterface;
use JohnSear\AccessControlBundle\Exception\NoValidFixtureException;

interface FixturesValidatorInterface
{
    public function setManager(ObjectManager $manager): FixturesValidatorInterface;

    public function find(FixtureEntityInterface $entity): ? FixtureEntityInterface;

    /**
     * @throws NoValidFixtureException
     */
    public function validate(FixtureEntityInterface $entity): bool;
}
