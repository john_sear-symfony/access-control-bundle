<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Fixtures\Base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use JohnSear\AccessControlBundle\Entity\User;
use JohnSear\AccessControlBundle\Entity\UserProfile;
use JohnSear\AccessControlBundle\Exception\NoValidFixtureException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

abstract class AbstractFixtures extends Fixture
{
    /** @var ObjectManager $manager */
    protected $manager;
    /** @var UserPasswordEncoderInterface  */
    protected $passwordEncoder;
    /** @var UserFixturesValidator */
    protected $userFixturesValidator;
    /** @var UserProfileFixturesValidator */
    private $userProfileFixturesValidator;

    public function __construct(
        UserPasswordEncoderInterface $passwordEncoder,
        UserFixturesValidator $userFixturesValidator,
        UserProfileFixturesValidator $userProfileFixturesValidator
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->userFixturesValidator = $userFixturesValidator;
        $this->userProfileFixturesValidator = $userProfileFixturesValidator;
    }

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->userFixturesValidator->setManager($manager);
        $this->userProfileFixturesValidator->setManager($manager);

        $this->initializeUserEntities();
        $manager->flush();

        $this->initializeUserProfileEntities();
        $manager->flush();
    }

    abstract protected function initializeUserEntities(): void;
    abstract protected function initializeUserProfileEntities(): void;

    /**
     * @throws NoValidFixtureException
     */
    protected function initUserOrSkip(User $user): void
    {
        $foundEntity = null;

        if($this->userFixturesValidator->validate($user)) {
            $foundEntity = $this->findUserByFixture($user);
        }

        if (! $foundEntity instanceof User) {
            $this->manager->persist($user);
        }
    }

    public function findUserByFixture(User $user): ?User
    {
        /** @var User $foundEntity */
        $foundEntity = $this->userFixturesValidator->find($user);

        return $foundEntity;
    }

    /**
     * @throws NoValidFixtureException
     */
    protected function initUserProfileOrSkip(UserProfile $userProfile): void
    {
        $foundEntity = null;

        if($this->userProfileFixturesValidator->validate($userProfile)) {
            $foundEntity = $this->findUserProfileByFixture($userProfile);
        }

        if (! $foundEntity instanceof UserProfile) {
            $this->manager->persist($userProfile);
        }
    }

    public function findUserProfileByFixture(UserProfile $userProfile): ?UserProfile
    {
        /** @var UserProfile $foundEntity */
        $foundEntity = $this->userProfileFixturesValidator->find($userProfile);

        return $foundEntity;
    }
}
