<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Fixtures\Base;

use Doctrine\Common\Persistence\ObjectManager;
use JohnSear\AccessControlBundle\Entity\FixtureEntityInterface;
use JohnSear\AccessControlBundle\Entity\User;
use JohnSear\AccessControlBundle\Entity\UserProfile;
use JohnSear\AccessControlBundle\Exception\NoValidFixtureException;
use JohnSear\AccessControlBundle\Repository\UserProfileRepository;

class UserProfileFixturesValidator implements FixturesValidatorInterface
{
    /** @var ObjectManager $manager */
    protected $manager;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        // .. initialization in AbstractUserFixtures

        $manager->flush();
    }

    public function setManager(ObjectManager $manager): FixturesValidatorInterface
    {
        $this->manager = $manager;

        return $this;
    }

    public function find(FixtureEntityInterface $entity): ? FixtureEntityInterface
    {
        /** @var User $user */
        $user = $entity->getUser();

        /** @var UserProfileRepository $userProfileRepository */
        $userProfileRepository = $this->manager->getRepository(UserProfile::class);

        return $userProfileRepository->findOneByUser($user);
    }

    /**
     * @throws NoValidFixtureException
     */
    public function validate(FixtureEntityInterface $userProfile): bool
    {
        if (
            is_null($userProfile->getUser())
        ) {
            throw new NoValidFixtureException('UserProfile Fixture contains not all necessary data.');
        }

        return true;
    }
}
