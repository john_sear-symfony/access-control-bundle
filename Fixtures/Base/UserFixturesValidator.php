<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Fixtures\Base;

use Doctrine\Common\Persistence\ObjectManager;
use JohnSear\AccessControlBundle\Entity\FixtureEntityInterface;
use JohnSear\AccessControlBundle\Entity\User;
use JohnSear\AccessControlBundle\Exception\NoValidFixtureException;
use JohnSear\AccessControlBundle\Repository\UserRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixturesValidator implements FixturesValidatorInterface
{
    /** @var ObjectManager $manager */
    protected $manager;
    /** @var UserPasswordEncoderInterface  */
    protected $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function setManager(ObjectManager $manager): FixturesValidatorInterface
    {
        $this->manager = $manager;

        return $this;
    }

    public function find(FixtureEntityInterface $entity): ? FixtureEntityInterface
    {
        /** @var User $user */
        $user = $entity;

        /** @var UserRepository $userRepository */
        $userRepository = $this->manager->getRepository(User::class);

        return $userRepository->findOneByEmail($user->getEmail());
    }

    /**
     * @throws NoValidFixtureException
     */
    public function validate(FixtureEntityInterface $user): bool
    {
        if (
            is_null($user->getEmail()) ||
            is_null($user->getPassword())
        ) {
            throw new NoValidFixtureException('User Fixture contains not all necessary data.');
        }

        return true;
    }
}
