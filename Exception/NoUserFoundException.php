<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Exception;

class NoUserFoundException extends \Exception
{
    
}
