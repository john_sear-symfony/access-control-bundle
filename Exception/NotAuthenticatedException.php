<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Exception;

class NotAuthenticatedException extends \Exception
{
    
}
