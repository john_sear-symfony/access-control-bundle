<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\UserResolver;

use JohnSear\AccessControlBundle\Exception\NotAuthenticatedException;
use JohnSear\AccessControlBundle\Exception\NoValidUserAuthenticatedException;
use JohnSear\AccessControlBundle\Entity\User;

interface UserResolverInterface
{
    /**
     * @throws NoValidUserAuthenticatedException
     * @throws NotAuthenticatedException
     */
    public function getCurrentUser(): User;

    public function hasRole(string $role): bool;
}
