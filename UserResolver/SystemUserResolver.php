<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\UserResolver;

use Doctrine\Common\Persistence\ObjectManager;
use JohnSear\AccessControlBundle\Exception\NoUserFoundException;
use JohnSear\AccessControlBundle\Resources\fixtures\SystemUserFixture;
use JohnSear\AccessControlBundle\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;

class SystemUserResolver
{
    /**
     * @throws NoUserFoundException
     */
    public static function getSystemUser(ObjectManager $manager): UserInterface
    {
        $systemRole = SystemUserFixture::SYSTEM_ROLE;

        $userRepository = $manager->getRepository(User::class);

        $foundUsers = $userRepository->findByRole($systemRole);

        $systemUser = reset($foundUsers);

        if (! $systemUser instanceof User) {
            throw new NoUserFoundException('System User not found.');
        }

        return $systemUser;
    }
}
