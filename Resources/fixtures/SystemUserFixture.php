<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle\Resources\fixtures;

use JohnSear\AccessControlBundle\Entity\UserProfile;
use JohnSear\AccessControlBundle\Exception\NoUserFoundException;
use JohnSear\AccessControlBundle\Exception\NoValidFixtureException;
use JohnSear\AccessControlBundle\Fixtures\Base\AbstractFixtures;
use JohnSear\AccessControlBundle\Entity\User;
use JohnSear\AccessControlBundle\UserResolver\SystemUserResolver;

class SystemUserFixture extends AbstractFixtures
{
    const SYSTEM_ROLE = 'ROLE_SYSTEM_USER';

    /**
     * @throws NoValidFixtureException
     */
    protected function initializeUserEntities(): void
    {
        $systemUser = (new User())
            ->setRoles([self::SYSTEM_ROLE])
            ->setEmail('info@die-staaken-herthaner.com')
        ;
        $systemUser->setPassword($this->passwordEncoder->encodePassword($systemUser, 'die-staaken-herthaner'));

        $this->initUserOrSkip($systemUser);
    }

    /**
     * @throws NoUserFoundException
     * @throws NoValidFixtureException
     */
    protected function initializeUserProfileEntities(): void
    {
        /** @var User $systemUser */
        $systemUser = SystemUserResolver::getSystemUser($this->manager);

        /** @var UserProfile $systemUserProfile */
        $systemUserProfile = (new UserProfile())
            ->setUser($systemUser)
            ->setFirstName('System')
            ->setLastName('User')
            ->setDateOfBirth(new \DateTime('1970-01-01 00:00:00'))
            ->setCurrentUser($systemUser)
        ;

        $this->initUserProfileOrSkip($systemUserProfile);
    }
}
