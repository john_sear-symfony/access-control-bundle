<?php declare(strict_types=1);

namespace JohnSear\AccessControlBundle;

use JohnSear\AccessControlBundle\DependencyInjection\AccessControlExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AccessControlBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new AccessControlExtension();
    }
}
